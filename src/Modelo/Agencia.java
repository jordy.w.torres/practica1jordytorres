/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Modelo;

/**
 *
 * @author Jordy
 */
public class Agencia {
    
   
  // La agencia esta conformada por 12 meses y numero de agencias por ende es como tener una matriz
   private int filas;
    private int columnas;
    

   public Agencia(int filas,int columnas){
       this.filas=filas;
       this.columnas=columnas;
   }
    
    public int getFilas() {
        return filas;
    }

    /**
     * @param filas the filas to set
     */
    public void setFilas(int filas) {
        this.filas = filas;
    }

    /**
     * @return the columnas
     */
    public int getColumnas() {
        return columnas;
    }

    /**
     * @param columnas the columnas to set
     */
    public void setColumnas(int columnas) {
        this.columnas = columnas;
    }
    
    
}
