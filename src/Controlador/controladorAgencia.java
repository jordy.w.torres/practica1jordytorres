/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Agencia;
import java.io.Serializable;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import java.io.FileWriter;
/**
 *
 * @author Jordy
 */
public class controladorAgencia {

    JSONArray jrr = new JSONArray();
    JSONArray jrr2 = new JSONArray();
     JSONObject obj = new JSONObject();
     JSONObject obj1 = new JSONObject(); 
     JSONObject obj2 = new JSONObject(); 
    
    private int[][] Matriz;
    private int filas;
    private int columnas;

    public controladorAgencia() {

    }
 // Constructor de la clase 
    public controladorAgencia(int filas, int columnas) {
        this.filas = filas;
        this.columnas = columnas;
        this.Matriz = new int[filas][columnas];
    }
// metodo para insertar la cantidad de filas,columnas y el dato mediante sus parametros
    public void insertar(int filas,int columas,int dato) {
        this.getMatriz()[filas][columas] = dato;
    }
// este metodo imprimir permite visualizar la matriz
    public String Imprimir() {
        String fila = "";
        String salto=",";
        for (int i = 0; i < this.filas; i++) {
            fila = fila + "[";
            for (int j = 0; j < this.columnas; j++) {
                fila = fila+" "+String.valueOf(Matriz[i][j])+salto;
            }
            fila = fila + "]";
        }
        return fila;

    }
       // metodo para obtener las ventas totales de la agencia segun su indice
    public int totalVentasAgencia(int indice) {
        int s = 0;
        for (int i = 0; i < this.columnas; i++) {
            if (i == indice) {
                for (int j = 0; j < this.filas; j++) {
                    s = s + Matriz[j][i];
                }
            }
        }
        return s;
    }
// la agencia con mayor ventas nos localiza tanto la agencia como el mes donde hubo mayores ventas
    public void agenciaMayorVentas() {
        int elementoMayor = Matriz[0][0];
         int posicionFila = 0;
        int posicionColumna = 0;
        for (int i = 0; i < this.filas; i++) {

            for (int j = 0; j < this.columnas; j++) {
                if (Matriz[i][j] > elementoMayor) {
                    elementoMayor = Matriz[i][j];
                    posicionFila = i;
                    posicionColumna = j;
                }
            }
        }
        JOptionPane.showMessageDialog(null, "La agencia" + "  " + posicionColumna + "   " + " del mes" + "  " + mostrarenMenu(posicionFila) + "  " + "es el mes con mas ventas");

    }
  //Este metodo nos permite obtener tanto su mes como la agencia que tuvo la menor cantidad de ventas 
    public void mesMenor() {
        int elementoMenor = Matriz[0][0];
        int posicionFila = 0;
        int posicionColumna = 0;
        for (int i = 0; i < this.filas; i++) {
            for (int j = 0; j < this.columnas; j++) {
                if (Matriz[i][j] < elementoMenor) {
                    elementoMenor = Matriz[i][j];
                    posicionFila = i;
                    posicionColumna = j;
                }
            }
        }
        JOptionPane.showMessageDialog(null, "La agencia" + "  " + posicionColumna + "   " + " en el mes" + "  " + mostrarenMenu(posicionFila) + "  " + "es el mes con menor ventas");
    }
//Nos sirve para llamar  a la matriz
    public int[][] getMatriz() {
        return Matriz;
    }

    public void setMatriz(int[][] Matriz) {
        this.Matriz = Matriz;
    }
// Insertamos el indice que es el numero del mes para obtener el promedio total de ventas de la agencia
    public int promedio(int indice) {
        int s = 0, promedio = 0;
        for (int i = 0; i < this.columnas; i++) {
            if (i == indice) {
                for (int j = 0; j < this.filas; j++) {
                    s = s + Matriz[j][i];

                }
            }

        }
        promedio = s / this.filas;
        return promedio;

    }
   
    // El metodo mostrar en Menu nos retorno un string que nos sirve para describir el nombre del mes segun su indie o posicion
      public String mostrarenMenu(int posicion){
          String Dato = null;
        switch (posicion) {
            case 0:
                Dato = "Enero";
                 break;
            case 1:
                Dato = "Febrero";
                break;
            case 2:
                Dato = "Marzo";
                break;
            case 3:
                Dato = "Abril";
                 break;
            case 4:
                Dato = "Mayo";
                break;
            case 5:
                Dato = "Junio";
                break;
            case 6:
                Dato = "Julio";
                break;
            case 7:
                Dato = "Agosto";
                break;
            case 8:
                Dato = "Septiembre";
               break;
            case 9:
                Dato = "Octubre";
               break;
            case 10:
                Dato = "Noviembre";
                break;
            case 11:
                Dato = "Diciembre";
                break;

            default:
                JOptionPane.showMessageDialog(null,"el mes no existe");
                break;
        }
        return Dato;
      }
      // metodos para guardar la matriz, el total de ventas y el promedio en archivo NOMBRE: Datos_Regsitro.json
     public void guardarMatriz(){
        String dato = Imprimir();
       
          obj.put("Datos",dato); 
                jrr.add(obj);
                //JOptionPane.showMessageDialog(null,jrr);
          try {
            FileWriter file = new FileWriter("Datos_Registro.json");
            file.write("En este documento se guardaran todos los registros por mes de todas las agencias\n");
            file.write(jrr.toJSONString()+"\n");
            file.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
         
      }
     public void guardarTotal(int indice){
        String total = String.valueOf(totalVentasAgencia(indice));
        
          obj1.put("Total",total);
        
         
                jrr.add(obj1);
                //JOptionPane.showMessageDialog(null,jrr);
          try {
            FileWriter file = new FileWriter("Datos_Registro.json");
            //file.write("En este documento se guardaran todos los registros por mes de todas las agencias\n");
            file.write(jrr.toJSONString());
            
            file.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
     }
         public void guardarPromedio(int indice){
        
        String promedio = String.valueOf(promedio(indice));

          obj2.put("Promedio", promedio);
                jrr.add(obj2);
                //JOptionPane.showMessageDialog(null,jrr);
          try {
            FileWriter file = new FileWriter("Datos_Registro.json");
            //file.write("En este documento se guardaran todos los registros por mes de todas las agencias\n");
            file.write(jrr.toJSONString()+"\n");
            file.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } 
         
      }
}
